# Backend
## Vai trò, mục đích xây dựng
- Backend sẽ thực hiện các thao tác với cơ sở dữ liệu với nhiệm vụ là quản lý về các file dịch thuật. 
- Ngoài ra, Backend phải được thiết kế để các dịch thuật viên có thể upload lên các file phụ đề tương ứng cho từng video trên các trang MOOC hoặc upload các tài liệu đã được dịch. Giúp các dịch thuật viên và quản trị viên có thể quản lý được các dữ liệu về dịch thuật, phụ đề. 
- Đồng thời cũng có cơ sở dữ liệu chứa danh sách các môn, link video, sô lượng truy cập từng video, …

## Các vai trò
- User: Đăng nhập, sử dụng, đăng xuất
- Translator: Upload file bài dịch, truy vấn các bài dịch, xem, sửa, xóa các bài dịch
- Admin: Thêm quyền, Xóa quyền của người dùng, xóa tài khoản người dùng