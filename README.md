# FUNiX Passport
## Tên dự án
- Assignment: Quản lý mã nguồn phần mềm FUNiX Passport
## Thông tin sinh viên
- Tên: Lê Minh Khôi
- Mã SV: fx28754
## Thông tin repo
- Mục đích: lưu trữ mã nguồn của phần mềm FUNiX Passport và các branch của nó
- Các branch:
    + bug/clear_console_log: branch gỡ lỗi in ra console
    + feat/auto_enable_subtitle: branch tính năng tự động bật subtitle
    + document: branch lưu document về dự án bằng markdown
    + backend: branch có thông tin về backend của dự án
## Thông tin khác
- Được fork từ repo [link](https://gitlab.com/anhndfx00424/funix-passport-assignment-swe)
- Ngày: 3/7/2024